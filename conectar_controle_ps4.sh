#!/bin/bash
reset

echo -e "Programa para iniciar a varredura de controles PS4 no Linux\n\n"


nome='ds4drv'
pacote=$(dpkg --get-selections | grep "$nome" ) 
echo 
echo -n "Verificando se o Pacote $nome esta instalado."
sleep 2
if [ -n "$pacote" ] ;
then echo
     echo "Pacote $nome ja instalado"
else echo
     echo "Pacote $nome Necessario-> Nao instalado"
     echo "Instalando Automaticamente Pacote Dialog..."
     sudo apt-get install $nome
fi

echo "Iniciando Varredura de dispositivos!!"

echo -e "Por favor, mantenha o botao SHARE + PSN precionados até começar a piscar as luzes no seu controle...\n"
echo -e "Para cancelar o processo, feche o terminal OU aperte Control + C\n\n"

sudo ds4drv
